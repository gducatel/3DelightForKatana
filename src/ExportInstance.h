/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2018                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#ifndef __ExportInstance_h
#define __ExportInstance_h

#include "SGLocation.h"

class ExportContext;

namespace ExportInstance
{
	void ExportHierarchicalInstance(
		ExportContext &i_ctx,
		const SGLocation &i_location );

	bool ExportIDInstance(
		ExportContext &i_ctx,
		const SGLocation &i_location,
		const std::string &i_nsi_node_type );

	void ExportInstanceArray(
		ExportContext &i_ctx,
		const SGLocation &i_location );
}

#endif
