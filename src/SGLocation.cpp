/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2017                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#include "SGLocation.h"

#include "AttributeUtils.h"

#include <assert.h>

/**
	\brief Build a location from a scene graph iterator.

	The iterator is held by reference so keep it valid.
*/
SGLocation::SGLocation( const FnKat::FnScenegraphIterator &i_iterator )
:
	m_location( i_iterator.getFullName() ),
	m_iterator( &i_iterator ),
	m_attributes( 0x0 ),
	m_deleted( 0 )
{
}

/**
	\brief Build a location from arbitrary attributes.

	The attribute group is held by reference so keep it valid.
*/
SGLocation::SGLocation(
	const std::string &i_location,
	const std::string &i_type,
	const FnAttribute::GroupAttribute &i_attributes )
:
	m_location( i_location ),
	m_type( i_type ),
	m_iterator( 0x0 ),
	m_attributes( &i_attributes ),
	m_deleted( 0 )
{
	/* Live updates for deletion contain only this attribute. */
	AttributeUtils::GetInteger( i_attributes, "deleted", m_deleted );
}

/**
	\brief Returns the type of the location

	eg. "group", "light", "polymesh", etc
*/
std::string SGLocation::getType() const
{
	if( m_attributes )
	{
		return m_type;
	}
	else
	{
		assert( m_iterator );
		return m_iterator->getType();
	}
}

std::string SGLocation::getParentFullName() const
{
	/*
		We could use the iterator's .getParent().getFullName() when present
		but I don't see the point. So we do it manually all the time.
	*/
	return m_location.substr( 0, m_location.find_last_of( '/' ) );
}

/**
	\brief Search for an attribute at the location.
*/
FnAttribute::Attribute SGLocation::getAttribute( const char *i_name ) const
{
	if( m_attributes )
	{
		return m_attributes->getChildByName( i_name );
	}
	else
	{
		assert( m_iterator );
		return m_iterator->getAttribute( i_name );
	}
}

/**
	\brief Returns a list of the attributes at this location.
*/
FnAttribute::StringAttribute SGLocation::getAttributeNames() const
{
	if( m_attributes )
	{
		std::vector<std::string> names;
		int64_t n = m_attributes->getNumberOfChildren();
		names.reserve( n );
		for( int64_t i = 0; i < n; ++i )
		{
			names.push_back( m_attributes->getChildName( i ) );
		}
		return FnAttribute::StringAttribute( names );
	}
	else
	{
		assert( m_iterator );
		return m_iterator->getAttributeNames();
	}
}

/**
	\brief Returns the root of the scene.

	This will not work on a live render update. It will return an invalid
	iterator. Do not use it, it is meant only to support legacy code which does
	not handle live updates anyway.
*/
FnKat::FnScenegraphIterator SGLocation::getRoot() const
{
	if( m_attributes )
	{
		return FnKat::FnScenegraphIterator();
	}
	else
	{
		assert( m_iterator );
		return m_iterator->getRoot();
	}
}
