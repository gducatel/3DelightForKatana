/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2016                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#include "XGenViewerModifier.h"

#ifdef _WIN32
#include <FnPlatform/Windows.h>
#endif

#include <FnViewerModifier/plugin/FnViewerModifier.h>
#include <FnViewerModifier/plugin/FnViewerModifierInput.h>
#include <FnAttribute/FnGroupBuilder.h>
#include <FnAttribute/FnAttribute.h>
#include <GL/gl.h>
#include <GL/glu.h>

#include <stdio.h>

#include "XGenLoader.h"

void DrawArchive(const Archive& i_archive)
{
	glEnable(GL_LIGHTING);
	glEnable(GL_CULL_FACE);
	glEnable(GL_NORMALIZE);

	bool normals = i_archive.normals.size() > 0;

	int counter = 0;
	for(int i=0; i<i_archive.npolys; i++)
	{
		int nvertices = i_archive.nvertices[i];

		glBegin(GL_TRIANGLE_FAN);

		for(int j=0; j<nvertices; j++)
		{
			if( normals )
			{
				const float* normal = &(i_archive.normals[counter*3]);
				glNormal3fv(normal);
			}

			int npoint = i_archive.vertices[counter];
			const float* point = &(i_archive.points[npoint*3]);
			glVertex3fv(point);

			counter++;
		}

		glEnd();
	}
}

XGenViewerModifier::XGenViewerModifier(FnKat::GroupAttribute args) :
	FnKat::ViewerModifier(args)
{
}

XGenViewerModifier::~XGenViewerModifier()
{
}

FnKat::ViewerModifier* XGenViewerModifier::create(
		FnKat::GroupAttribute args)
{
	return new XGenViewerModifier(args);
}

FnKat::GroupAttribute XGenViewerModifier::getArgumentTemplate()
{
	FnKat::GroupBuilder gb;
	return gb.build();
}

const char* XGenViewerModifier::getLocationType()
{
	return "XGen";
}

void XGenViewerModifier::deepSetup(FnKat::ViewerModifierInput& input)
{
}

void XGenViewerModifier::setup(FnKat::ViewerModifierInput& input)
{
	LoadGetXGenGeoID(input);
}

void XGenViewerModifier::draw(FnKat::ViewerModifierInput& input)
{
	FnKat::DoubleAttribute xformAttr = input.getWorldSpaceXform();
	FnKat::DoubleConstVector xform = xformAttr.getNearestSample(0.f);

	glPushAttrib(
			GL_CURRENT_BIT | GL_POLYGON_BIT | GL_LIGHTING_BIT | GL_LINE_BIT);
	glPushMatrix();

	glDisable(GL_LIGHTING);
	glLineWidth(0.5f);

	if(input.isSelected())
	{
		/* Selected. White. */
		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	}
	else
	{
		/* Not selected. Gray. */
		glColor4f(0.5f, 0.5f, 0.5f, 1.0f);
	}

	FnKat::FloatAttribute density_attr = input.getAttribute("xgen.density");
	float density = density_attr.getValue();

	int geoid = LoadGetXGenGeoID(input);
	if( geoid>=0 )
	{
		const Hair& hair = XGenLoader::GetGeo(geoid);

		int counter = 0;
		for(int j = 0; j<hair.npoints.size(); j++)
		{
			/* Filter strands */
			if( hair.random[j] > density )
			{
				counter += hair.npoints[j];
				continue;
			}

			glBegin(GL_LINE_STRIP);

			for(int i=0; i<hair.npoints[j]; i++)
			{
				vec3 p = hair.points[counter++];
				glVertex3f(p.x, p.y, p.z);
			}

			glEnd();
		}

		/* Draw archives */
		for(int j = 0; j < hair.archives.size(); j++)
		{
			const Archive& archive = XGenLoader::GetArchive(hair.archives[j]);
			if( !archive.valid )
			{
				continue;
			}

			glPushMatrix();

			glMultMatrixf(&(hair.transforms[j]._00));

			/* Draw archive */
			DrawArchive(archive);

			glPopMatrix();
		}
	}

	glPopMatrix();
	glPopAttrib();
}

void XGenViewerModifier::cleanup(FnKat::ViewerModifierInput& input)
{
	ClearXGenCache(input);
}

void XGenViewerModifier::deepCleanup(FnKat::ViewerModifierInput& input)
{
}

FnKat::DoubleAttribute XGenViewerModifier::getLocalSpaceBoundingBox(
		FnKat::ViewerModifierInput& input)
{
	int geoid = LoadGetXGenGeoID(input);
	if( geoid>=0 )
	{
		double bounds[6];
		/* Get BBox from the cache. */
		XGenLoader::GetBBox(geoid, bounds);
		return FnKat::DoubleAttribute(bounds, 6, 1);
	}
	else
	{
		double radius = 1.0;
		double bounds[6] = {-radius, radius, -radius, radius, -radius, radius};
		return FnKat::DoubleAttribute(bounds, 6, 1);
	}
}

void XGenViewerModifier::flush()
{
}

void XGenViewerModifier::onFrameBegin()
{
}

void XGenViewerModifier::onFrameEnd()
{
}

int XGenViewerModifier::LoadGetXGenGeoID(
		const FnKat::ViewerModifierInput& input)
{
	FnKat::StringAttribute xgenlibdir = input.getAttribute("xgen.xgenlibdir");
	if( XGenLoader::Setup(xgenlibdir.getValue()) )
	{
		FnKat::StringAttribute file = input.getAttribute("xgen.file");
		FnKat::StringAttribute palette = input.getAttribute("xgen.palette");
		FnKat::StringAttribute desc = input.getAttribute("xgen.description");
		FnKat::StringAttribute patch = input.getAttribute("xgen.patch");
		FnKat::StringAttribute geom = input.getAttribute("xgen.geom");
		FnKat::FloatAttribute frametime = input.getAttribute("xgen.frametime");
		return XGenLoader::GetGeoID(
					file.getValue(),
					palette.getValue(),
					desc.getValue(),
					patch.getValue(),
					geom.getValue(),
					(int)frametime.getValue() );
	}

	return -1;
}

void XGenViewerModifier::ClearXGenCache(
		const FnKat::ViewerModifierInput& input)
{
	FnKat::StringAttribute file = input.getAttribute("xgen.file");
	FnKat::StringAttribute palette = input.getAttribute("xgen.palette");
	FnKat::StringAttribute desc = input.getAttribute("xgen.description");
	FnKat::StringAttribute patch = input.getAttribute("xgen.patch");
	FnKat::StringAttribute geom = input.getAttribute("xgen.geom");

	XGenLoader::Clear(
			file.getValue(),
			palette.getValue(),
			desc.getValue(),
			patch.getValue(),
			geom.getValue() );
}

