/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2017                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#include "AttributeUtils.h"

namespace AttributeUtils
{

void GetString(
	const FnAttribute::Attribute &i_attr,
	std::string &io_value )
{
	if( !i_attr.isValid() )
		return;

	FnAttribute::StringAttribute string_attr( i_attr );
	if( !string_attr.isValid() )
		return;

	FnAttribute::StringAttribute::array_type v =
		string_attr.getNearestSample( 0.0f );

	if( !v.empty() )
		io_value = v[0];
}

void GetInteger(
	const FnAttribute::Attribute &i_attr,
	int &io_value )
{
	if( !i_attr.isValid() )
		return;

	FnAttribute::IntAttribute int_attr( i_attr );
	if( !int_attr.isValid() )
		return;

	FnAttribute::IntAttribute::array_type v =
		int_attr.getNearestSample( 0.0f );

	if( !v.empty() )
		io_value = v[0];
}

void GetFloat(
	const FnAttribute::Attribute &i_attr,
	float &io_value )
{
	if( !i_attr.isValid() )
		return;

	FnAttribute::FloatAttribute float_attr( i_attr );
	if( !float_attr.isValid() )
		return;

	FnAttribute::FloatAttribute::array_type v =
		float_attr.getNearestSample( 0.0f );

	if( !v.empty() )
		io_value = v[0];
}

/*
	Be careful when using this as it does not support motion blur.

	ExportSG::ExportFloatAttribute is far better when a direct export is
	possible.
*/
void GetDouble(
	const FnAttribute::Attribute &i_attr,
	double &io_value )
{
	if( !i_attr.isValid() )
		return;

	FnAttribute::DoubleAttribute double_attr( i_attr );
	if( !double_attr.isValid() )
		return;

	FnAttribute::DoubleAttribute::array_type v =
		double_attr.getNearestSample( 0.0f );

	if( !v.empty() )
		io_value = v[0];
}

}
