/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2016                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#include "LogReporting.h"

#include "utils.h"

#include <stdarg.h>
#include <stdio.h>

void LogMessage(
		const char* i_func, int i_severity, const char* i_message, ...)
{
	if( i_severity < 0 )
	{
		return;
	}

	va_list arguments;

	printf("[%s] ", _3DELIGHT_RENDERER_NAME);
	switch( i_severity )
	{
		case eLevelInfo:
			printf("Info: ");
			break;
		case eLevelWarning:
			printf("Warning: ");
			break;
		case eLevelError:
			printf("Error: ");
			break;
		case eLevelFatal:
			printf("Fatal: ");
			break;
	}
	printf("%s: ", i_func);

	va_start (arguments, i_message);
	vprintf (i_message, arguments);
	va_end (arguments);

	printf("\n");
}

