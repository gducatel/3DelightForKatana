/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2016                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#include "XGenLoader.h"
#include "LogReporting.h"
#include "utils.h"

#include <fstream>

#ifndef _WIN32
#include <dlfcn.h>
#endif

using namespace XGenRenderAPI::Utils;

/* Static variables. */
std::vector<Hair> XGenLoader::gs_cache;
std::vector<Archive> XGenLoader::gs_archives;
HMODULE XGenLoader::gs_libXGen = NULL;
PatchRenderer* (*XGenLoader::gs_patchrender_init)(
		ProceduralCallbacks*, char const*) = NULL;
FaceRenderer* (*XGenLoader::gs_facerender_init)(
		PatchRenderer*, unsigned int, ProceduralCallbacks*) = NULL;

/* Converts strange XGen points to the transformation matrices. */
mat44 PointsToMatrix(const vec3* i_P, double i_bbox_scale, bool i_normal_param)
{
	vec3 P = i_P[0];
	vec3 lengthP( i_P[1] );
	vec3 midP(( P + lengthP )/2.0 );
	vec3 widthP( i_P[2] );
	vec3 depthP( i_P[3] );
	vec3 lengthVec = lengthP - P;
	vec3 widthVec = widthP - midP;
	double length_ = length(lengthVec);
	double width = length( widthVec ) * 2.0;
	double depth = length(depthP - midP) * 2.0;

	// Determine axis and angle of rotation.
	vec3 yAxis={ 0.0, 1.0, 0.0 };
	vec3 xAxis={ 1.0, 0.0, 0.0 };
	vec3 zeroAxis = { 0.f, 0.f, 0.f };
	vec3 xChange;

	vec3 axis1 = yAxis * lengthVec;
	double angle1;
	if ( normalize(axis1) > 0.0 )
	{
		angle1 = angle(yAxis, lengthVec );
		xChange = rotateBy(xAxis, axis1, angle1 );
	}
	else
	{
		angle1 = 0.0;
		axis1 = xAxis;
		xChange = xAxis;
	}

	vec3 axis2 = xChange * widthVec;
	double angle2;
	if ( normalize(axis2) > 0.0 )
	{
		angle2 = angle( xChange, widthVec );
		if ( dot( axis2, lengthVec ) < 0.0 )
		{
			angle2 *= -1.0;
		}
	}
	else
	{
		angle2 = 0.0;
	}
	axis2 = yAxis;

	mat44 xP, xN, tmp;

	// Translation
	translation( tmp, P );
	xP = tmp;

	// Rotation 1
	if ( axis1 != zeroAxis )
	{
		rotation( tmp, axis1, angle1 );
		multiply( xP, xP, tmp );
		if ( i_normal_param )
		{
			xN = tmp;
		}
	}

	// Rotation 2
	if ( axis2 != zeroAxis )
	{
		rotation( tmp, axis2, angle2 );
		multiply( xP, xP, tmp );

		if ( i_normal_param )
		{
			multiply( xN, xN, tmp );
		}
	}

	// Scale
	vec3 scaleV;
	scaleV.x = i_bbox_scale * width;
	scaleV.y = i_bbox_scale * length_;
	scaleV.z = i_bbox_scale * depth;
	scale( tmp, scaleV );
	multiply( xP, xP, tmp );

	return xP;
}

void getRIBPart(const char* i_filename, size_t& o_first, size_t& o_last )
{
	/* Splits the string to the sub-string using separator '\n'. Returns start
	   and end of the first substring that ends with ".rib\n". Otherwise, it
	   should return the latest substring. RIB filename should always end with
	   ".rib" (case-insensitive). */
	size_t length = strlen(i_filename);
	size_t first = 0;
	size_t last = 0;

	/* Looking for \n in the string */
	while(last<length)
	{
		if(i_filename[last] == '\n')
		{
			/* Found a substring */
			if( last-first > 4 )
			{
				if( toupper(i_filename[last-3]) == 'R' &&
					toupper(i_filename[last-2]) == 'I' &&
					toupper(i_filename[last-1]) == 'B' )
				{
					/* It has 'rib' at the end. We found it. */
					break;
				}
			}

			first = ++last;
		}

		last++;
	}

	o_first = first;
	o_last = last;
}

RendermanCallbacks::RendermanCallbacks(Hair& i_cache) :
	m_cache(i_cache),
	m_shutter( 0.0f )
{
}

RendermanCallbacks::~RendermanCallbacks()
{
}

void RendermanCallbacks::flush(  const char* in_geom, PrimitiveCache* in_cache )
{
	bool is_spline =
		in_cache->get( XGenRenderAPI::PrimitiveCache::PrimIsSpline );
	const char* strPrimType =
		in_cache->get( XGenRenderAPI::PrimitiveCache::PrimitiveType );

	if( is_spline )
	{
		FlushSplines( in_geom, in_cache );
	}
	else if ( strcmp( strPrimType, "CardPrimitive" )==0 )
	{
		FlushCards( in_geom, in_cache );
	}
	else
	{
		printf("Unknown primitive %s\n", strPrimType);
	}
}

void RendermanCallbacks::FlushSplines(
		const char* in_geom, PrimitiveCache* in_cache )
{
	unsigned int ncurves =
		in_cache->get( XGenRenderAPI::PrimitiveCache::CacheCount );
	const vec3* points =
		in_cache->get( XGenRenderAPI::PrimitiveCache::Points, 0 );
	const int* nvertices =
		in_cache->get( XGenRenderAPI::PrimitiveCache::NumVertices, 0 );
	const float* random =
		in_cache->get( XGenRenderAPI::PrimitiveCache::RandomFloat_XP );

	int npoints = 0;
	for(unsigned int i=0; i<ncurves; i++)
	{
		m_cache.npoints.push_back(nvertices[i]);
		m_cache.random.push_back(random[i]);

		for(int j=0; j<nvertices[i]; j++)
		{
			const vec3& p = points[npoints++];

			/* Compute bounding box */
			if( m_cache.points.empty() )
			{
				m_cache.bbox[0] = p.x;
				m_cache.bbox[1] = p.x;
				m_cache.bbox[2] = p.y;
				m_cache.bbox[3] = p.y;
				m_cache.bbox[4] = p.z;
				m_cache.bbox[5] = p.z;
			}
			else
			{
				m_cache.bbox[0] = std::min(p.x, m_cache.bbox[0]);
				m_cache.bbox[1] = std::max(p.x, m_cache.bbox[1]);
				m_cache.bbox[2] = std::min(p.y, m_cache.bbox[2]);
				m_cache.bbox[3] = std::max(p.y, m_cache.bbox[3]);
				m_cache.bbox[4] = std::min(p.z, m_cache.bbox[4]);
				m_cache.bbox[5] = std::max(p.z, m_cache.bbox[5]);
			}

			/* Save it */
			m_cache.points.push_back(p);
		}
	}
}

void RendermanCallbacks::FlushCards(
		const char* in_geom, PrimitiveCache* in_cache )
{
	/* Todo: Add cards support */
}

void RendermanCallbacks::log( const char* in_str )
{
	LOGVAMESSAGE(eLevelInfo, "XGen: %s", in_str);
}

bool RendermanCallbacks::get( EBoolAttribute in_attr ) const
{
	return false;
}

const char* RendermanCallbacks::get( EStringAttribute i_attr ) const
{
	/* TODO: fill it */
	if( i_attr == RenderCam )
	{
		return "false, 0.0 0.0 0.0";
	}
	else if( i_attr == RenderCamFOV )
	{
		return "90, 90";
	}
	else if( i_attr == RenderCamXform )
	{
		return
			"1.0 0.0 0.0 0.0 "
			"0.0 1.0 0.0 0.0 "
			"0.0 0.0 1.0 0.0 "
			"0.0 0.0 0.0 1.0";
	}
	else if( i_attr == RenderCamRatio )
	{
		return "1.0";
	}

	return "";
}

float RendermanCallbacks::get( EFloatAttribute ) const
{
	/* TODO: fill it */
	return 0.f;
}

const float* RendermanCallbacks::get( EFloatArrayAttribute in_attr ) const
{
	if( in_attr==Shutter )
	{
		/* Without shutter XGen doesn't reload the collection that was
		   previously loaded with a different frame. */
		return &m_shutter;
	}

	/* TODO: fill it */
	return NULL;
}

unsigned int RendermanCallbacks::getSize( EFloatArrayAttribute in_attr ) const
{
	/* TODO: fill it */
	if( in_attr==Shutter )
	{
		return 1;
	}

	return 0;
}

const char* RendermanCallbacks::getOverride( const char* in_name ) const
{
	return "";
}

bool RendermanCallbacks::getArchiveBoundingBox(
		const char* in_filename, bbox& out_bbox ) const
{
	/* TODO: fill it */
	out_bbox.xmin = 0.f;
	out_bbox.xmax = 0.f;
	out_bbox.ymin = 0.f;
	out_bbox.ymax = 0.f;
	out_bbox.zmin = 0.f;
	out_bbox.zmax = 0.f;

	return true;
}

void RendermanCallbacks::getTransform( float in_time, mat44& out_mat ) const
{
}

bool XGenLoader::Setup(const std::string& i_xgen_dir)
{
	if( gs_libXGen && gs_patchrender_init && gs_facerender_init )
	{
		return true;
	}

#ifdef _WIN32
	/* We need to add path to libPtex to PATH. */
	std::string ptexdir = i_xgen_dir + "..\\..\\bin";
	::Utils::EnsurePathInEnvironment("PATH", ptexdir);
#else
	/*
		Load libAdskXGen's dependencies which are in Maya's lib folder first.
		We don't check for failure as we'll get an error message later anyway.
	*/
	const char* xgen_deps[] =
		{ "libPtex.so", "libtbb.so.2", "libtbbmalloc.so.2", "libclew.so" };
	for( auto dep : xgen_deps )
	{
		dlopen((i_xgen_dir + "../../lib/" + dep).c_str(), RTLD_NOW|RTLD_GLOBAL);
	}
#endif

#ifdef _WIN32
	std::string xgendir = i_xgen_dir + "bin";
	::Utils::EnsurePathInEnvironment("PATH", xgendir);
	std::string xgenlibname = "libAdskXGen.dll";
	std::string xgenlibpath = xgendir + "/" + xgenlibname;
	/* Test if that lib exists. */
	if( INVALID_FILE_ATTRIBUTES == GetFileAttributes(xgenlibpath.c_str()) )
	{
		/* Maya 2019 and up removed the "lib" from the library names. */
		xgenlibname = "AdskXGen.dll";
	}
	gs_libXGen = LoadLibrary(xgenlibname.c_str());
#else
	std::string xgenlib = i_xgen_dir + "lib/libAdskXGen.so";
	gs_libXGen = dlopen(xgenlib.c_str(), RTLD_NOW | RTLD_GLOBAL);
#endif
	if(!gs_libXGen)
	{
#ifdef _WIN32
		LOGVAMESSAGE(eLevelError, "Can't load xgen, error is %i", GetLastError());
#else
		LOGVAMESSAGE(eLevelError, "Can't load xgen, %s", dlerror());
#endif
		return false;
	}

	/* Clear any existing error */
#ifdef _WIN32
	GetLastError();
#else
	dlerror();
#endif

#ifdef _WIN32
	/* Load PatchRenderer::init */
	const char* patch_render_init_symbol =
		"?init@PatchRenderer@XGenRenderAPI@"
		"@SAPEAU12@PEAUProceduralCallbacks@2@PEBD@Z";
	*(void **) (&gs_patchrender_init) =
		GetProcAddress(gs_libXGen, patch_render_init_symbol);
#else
	/* Load PatchRenderer::init */
	const char* patch_render_init_symbol =
		"_ZN13XGenRenderAPI13PatchRenderer4initEPNS_19ProceduralCallbacksEPKc";
	*(void **) (&gs_patchrender_init) =
		dlsym(gs_libXGen, patch_render_init_symbol);
#endif

	if(!gs_patchrender_init)
	{
#ifdef _WIN32
		LOGVAMESSAGE(
			eLevelError,
			"Can't load PatchRenderer::init, error is %i",
			GetLastError());
#else
		LOGVAMESSAGE(
				eLevelError, "Can't load PatchRenderer::init, %s", dlerror());
#endif
		return false;
	}

#ifdef _WIN32
	/* Load FaceRenderer::init */
	const char* face_render_init_symbol =
		"?init@FaceRenderer@XGenRenderAPI@"
		"@SAPEAU12@PEAUPatchRenderer@2@IPEAUProceduralCallbacks@2@@Z";
	*(void **) (&gs_facerender_init) =
		GetProcAddress(gs_libXGen, face_render_init_symbol);
#else
	/* Load FaceRenderer::init */
	const char* face_render_init_symbol =
		"_ZN13XGenRenderAPI12FaceRenderer4initEPNS"
		"_13PatchRendererEjPNS_19ProceduralCallbacksE";
	*(void **) (&gs_facerender_init) =
		dlsym(gs_libXGen, face_render_init_symbol);
#endif

	if(!gs_facerender_init)
	{
#ifdef _WIN32
		LOGVAMESSAGE(
			eLevelError,
			"Can't load FaceRenderer::init, error is %i",
			GetLastError());
#else
		LOGVAMESSAGE(
				eLevelError, "Can't load FaceRenderer::init, %s", dlerror());
#endif
		return false;
	}

	return true;
}

int XGenLoader::GetGeoID(
		const std::string& i_file,
		const std::string& i_palette,
		const std::string& i_desc,
		const std::string& i_patch,
		const std::string& i_geom,
		int i_frame)
{
	/* Check if we already have it in the cache */
	unsigned int cachesize = (unsigned int)gs_cache.size();
	int invalid = -1;
	for(unsigned int i=0; i<cachesize; i++)
	{
		Hair& hair = gs_cache[i];
		if( hair.file == i_file &&
			hair.palette == i_palette &&
			hair.desc == i_desc &&
			hair.patch == i_patch &&
			hair.geom == i_geom &&
			hair.frame == i_frame )
		{
			if( hair.valid )
			{
				return i;
			}
			else if( invalid < 0 )
			{
				invalid = i;
			}
		}
	}

	/* Check if we have XGen functions. */
	if( !gs_patchrender_init || !gs_facerender_init )
	{
		return -1;
	}

	unsigned int id;

	if( invalid >= 0 )
	{
		id = invalid;
	}
	else
	{
		id = (unsigned int)gs_cache.size();

		/* Create an entry in the cache */
		gs_cache.push_back( Hair() );
	}

	Hair& hair = gs_cache[id];
	hair.file=i_file;
	hair.palette=i_palette;
	hair.desc=i_desc;
	hair.patch=i_patch;
	hair.geom=i_geom;
	hair.frame=i_frame;
	hair.valid=true;

	/* Standard XGenRenderAPI routine. */
	RendermanCallbacks* cb = new RendermanCallbacks(hair);

	/* Frame number as string */
	char frame[256];
	sprintf(frame, "%i", i_frame);

	/* Parameter string */
	std::string paramstr =
		std::string("-file ") + i_file +
		" -palette " + i_palette +
		" -description " + i_desc +
		" -patch " + i_patch +
		" -frame " + frame +
		" -fps 24"
		" -geom " + i_geom;
	PatchRenderer* patch = gs_patchrender_init( cb, paramstr.c_str() );

	if( !patch )
	{
		fprintf( stderr,
			"XGen: error loading file '%s' or geometry '%s'\n",
			i_file.c_str(),
			i_geom.c_str() );

		return id;
	}

	bbox b;
	unsigned int f = -1;
	while( patch->nextFace( b, f ) )
	{
		/* Skip camera culled bounding boxes. */
		if( XGenRenderAPI::Utils::isEmpty( b ) )
		{
			continue;
		}

		RendermanCallbacks* ncb = new RendermanCallbacks(hair);

		FaceRenderer* face = gs_facerender_init( patch, f, ncb );

		/* This will call RendermanCallbacks.flush */
		face->render();
	}

	return id;
}

void XGenLoader::GetBBox(int id, double o_bbox[6])
{
	for(int i=0; i<6; i++)
	{
		o_bbox[i] = gs_cache[id].bbox[i];
	}
}

const Hair& XGenLoader::GetGeo(int id)
{
	return gs_cache[id];
}

void XGenLoader::Clear(int id)
{
	Hair& hair = gs_cache[id];
	hair.points.clear();
	hair.npoints.clear();
	hair.random.clear();
	hair.valid = false;

	/* Clear archives */
	/* We need the archive ids to clean */
	std::set<int> archives_to_clean;
	std::vector<int>::iterator it;
	for( it = hair.archives.begin(); it != hair.archives.end(); ++it )
	{
		archives_to_clean.insert(*it);
	}

	/* We shouldn't clean archives that are used by other hair. So we need to
	   check each element in the cache. */
	for( int i=0; i<gs_cache.size(); i++ )
	{
		if(archives_to_clean.empty())
		{
			break;
		}

		Hair& other_hair = gs_cache[i];
		if( !other_hair.valid )
		{
			/* Current item should be skipped because it's not valid anymore */
			continue;
		}

		/* Checking each element in the cache */
		for(it=other_hair.archives.begin(); it!=other_hair.archives.end(); ++it)
		{
			/* Find an archive in out list to clean */
			std::set<int>::iterator found = archives_to_clean.find(*it);
			if( found != archives_to_clean.end() )
			{
				/* Erase it. */
				archives_to_clean.erase(found);
			}
		}
	}

	/* Now we can erase everything in the archive */
	std::set<int>::iterator arid;
	for(arid=archives_to_clean.begin(); arid!=archives_to_clean.end(); ++arid)
	{
		Archive& archive = gs_archives[*arid];

		archive.nvertices.clear();
		archive.vertices.clear();
		archive.points.clear();
		archive.normals.clear();

		archive.valid = false;
	}

	hair.archives.clear();
	hair.transforms.clear();
	hair.archive_randoms.clear();
}

void XGenLoader::Clear(
		const std::string& i_file,
		const std::string& i_palette,
		const std::string& i_desc,
		const std::string& i_patch,
		const std::string& i_geom)
{
	/* Check if we already have it in the cache */
	unsigned int cachesize = (unsigned int)gs_cache.size();
	for(unsigned int i=0; i<cachesize; i++)
	{
		Hair& hair = gs_cache[i];
		if( hair.file == i_file &&
			hair.palette == i_palette &&
			hair.desc == i_desc &&
			hair.patch == i_patch &&
			hair.geom == i_geom )
		{
			Clear(i);
		}
	}
}

int XGenLoader::GetArchiveID(
		const std::string& i_archive)
{
	/* Check if we already have it in the cache */
	unsigned int cachesize = (unsigned int)gs_archives.size();
	for(unsigned int i=0; i<cachesize; i++)
	{
		Archive& archive = gs_archives[i];
		if( archive.name == i_archive )
		{
			return i;
		}
	}

	/* Create an entry in the cache */
	gs_archives.push_back( Archive() );
	Archive& archive = gs_archives.back();

	archive.name = i_archive;
	archive.valid = false;

	return cachesize;
}

Archive& XGenLoader::GetArchive(int id)
{
	return gs_archives[id];
}
