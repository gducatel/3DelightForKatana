/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2017                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#ifndef __ExportContext_h
#define __ExportContext_h

#include "FnScenegraphIterator/FnScenegraphIterator.h"
#include "CacheEvictionManager.h"
#include "GlobalSettings.h"
#include "nsi.hpp"
#include "RenderSettings.h"
#include "tbb/enumerable_thread_specific.h"

#include <map>
#include <mutex>
#include <string>

class Renderer;

class ExportContext
{
	ExportContext( const ExportContext& );
	void operator=( const ExportContext& );

public:
	ExportContext(
		const NSI::DynamicArgumentList &i_nsi_begin_args,
		const FnKat::FnScenegraphIterator &i_rootIterator,
		Renderer &i_renderer );

	std::string OutputDriverHandle(
		const std::string &i_filename,
		const std::string &i_drivername,
		bool *o_first_request );

	std::string GenOutputLayerHandle();

	void FirstIDInstancePath(
		const std::string &i_ID,
		std::string &io_path,
		const std::string &i_nsi_node_type );

	void AddScreen( const std::string &i_handle );
	void RemoveScreen( const std::string &i_handle );

	void ChangeScreenForLayers(
		const std::string &i_oldScreen,
		const std::string &i_newScreen );

	/* Accessing this list is not thread safe. */
	typedef std::set<std::string> tScreenList;
	const tScreenList& ScreenList() const { return m_screens; }

	CacheEvictionManager& EvictionManager();

public:
	NSI::Context m_nsi;

	/* Renderer instance doing the export. */
	Renderer &m_renderer;

	/* Global and render settings. */
	GlobalSettings m_globalSettings;
	RenderSettings m_renderSettings;

private:
	/* Mutex for the stuff below. */
	std::mutex m_mutex;

	/* map of output drivers */
	std::map<std::string, std::string> m_output_driver_map;

	/* output layer count for naming */
	unsigned m_output_layer_count;

	/* This is a mess but at least it's a private mess. */
	class Instance
	{
		void operator=( const Instance& );
	public:
		Instance() {}
		Instance( const Instance &r ) : m_master_path(r.m_master_path) {}
		/* Path of copy of geo which is instanced. */
		std::string m_master_path;
	};
	/* map of instances by ID */
	std::map<std::string, Instance> m_instance_map;

	/* All the exported screens. */
	tScreenList m_screens;

	/* TLS for the cache eviction manager. */
	tbb::enumerable_thread_specific<CacheEvictionManager> m_cem_tls;
};

#endif
