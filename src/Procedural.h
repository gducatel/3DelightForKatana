/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2016                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#ifndef __procedural_h
#define __procedural_h

#include "nsi.hpp"

#include "OSLShaderInfo.h"
#include "SGLocation.h"

class ExportContext;

namespace Procedural
{
	typedef std::pair<std::string, FnAttribute::Attribute> shader_arg_t;

	void ProcessAllShaderAttributes(
			const DlShaderInfo& i_shader,
			const std::vector<shader_arg_t> &i_args,
			NSI::DynamicArgumentList& io_attributes );

	void ProcessDefaultConnection(
		ExportContext &i_ctx,
		const SGLocation &i_location,
		const DlShaderInfo& i_osl_shader,
		const DlShaderInfo::Parameter* i_osl_param,
		const std::string& i_shader_handle,
		std::vector<std::string>& io_createdNodeNames );

	/** Writes the shading network. */
	void WriteNSI_Shading_Network(
		ExportContext &i_ctx,
		const SGLocation &i_sgIterator,
		const std::string &i_material_group );
};

#endif

