/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2017                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#ifndef __ExportGeo_h
#define __ExportGeo_h

#include "SGLocation.h"

class ExportContext;

/*
	This namespace contains code to export geometry nodes.
*/
namespace ExportGeo
{
	void ExportID(
		ExportContext &i_ctx,
		const SGLocation &i_location );

	void ExportFaceSet(
		ExportContext &i_ctx,
		const SGLocation &i_location );

	bool ExportPolymesh(
		ExportContext &i_ctx,
		const SGLocation &i_location );

	void ExportSubdivmesh(
		ExportContext &i_ctx,
		const SGLocation &i_location );

	void ExportCurves(
		ExportContext &i_context,
		const SGLocation &i_location );

	void ExportPointCloud(
		ExportContext &i_context,
		const SGLocation &i_location );

	void ExportLightShape(
		ExportContext &i_ctx,
		const SGLocation &i_location,
		const std::string &i_shape );

	void ExportXGen(
		ExportContext& i_ctx,
		const SGLocation &i_location );

	void ExportYeti(
		ExportContext& i_ctx,
		const SGLocation &i_location );

	void ExportNSIArchive(
		ExportContext& i_ctx,
		const SGLocation &i_location );

	void ExportOpenVDB(
		ExportContext& i_ctx,
		const SGLocation &i_location );
	
	void ExportAtmosphere(
		ExportContext &i_ctx,
		const SGLocation &i_location );

	void ExportArbitraryValues(
		ExportContext &i_ctx,
		const SGLocation &i_location,
		const FnAttribute::IntAttribute &i_vertexList );

	void ExportOneArbitraryValue(
		ExportContext &i_ctx,
		const SGLocation &i_location,
		const FnAttribute::IntAttribute &i_vertexList,
		const std::string &i_value_name,
		const FnAttribute::GroupAttribute &i_value );

	void ExportAlembicUserAttributes(
		ExportContext &i_ctx,
		const SGLocation &i_location );
}

#endif

