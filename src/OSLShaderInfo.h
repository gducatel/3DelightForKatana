/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2017                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#ifndef __OSLShaderInfo
#define __OSLShaderInfo

#include "3Delight/ShaderQuery.h"

#include <string>
#include <vector>

/*
	This is a wrapper around OSLQuery to make using the contained information
	easier and more efficient. Long term goals include things such as efficient
	search through shader parameters.
*/
class OSLShaderInfo : public DlShaderInfo
{
public:
	bool open( const char *i_name );

	/* Returns true if the shader has the given tag in its metadata. */
	bool HasTag( const std::string &i_tag ) const;

private:
	/* vector of shader metadata 'tags'. This is not a ustring on purpose: I
	   don't want ustring to be built to compare against these values. */
	std::vector<std::string> m_tags;
};

#endif
