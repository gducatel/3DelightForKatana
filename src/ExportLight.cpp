/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2017                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#include "ExportLight.h"

#include "AttributeUtils.h"
#include "ExportAttributes.h"
#include "ExportContext.h"
#include "ExportGeo.h"
#include "ExportMaterial.h"
#include "ExportSG.h"

using AttributeUtils::GetDouble;
using AttributeUtils::GetString;

namespace ExportLight
{

void ExportLightLocation(
	ExportContext &i_ctx,
	const SGLocation &i_location )
{
	// I think we need to check info.light.mute and info.light.solo
	// Make it edit friendly... like export the lights but don't connect them.

	std::string light_name = i_location.getFullName();

	if( i_location.IsDeleted() )
	{
		/* Wipe everything. */
		i_ctx.m_nsi.Delete( light_name, NSI::IntegerArg( "recursive", 1 ) );
		return;
	}

	if( i_location.IsLiveUpdate() && !i_location.IsDeleted() )
	{
		/*
			Look for 'dl_light_parents' group which packages all parents up to
			the root. This is needed for the render working set changes which
			will send a new light without any of its parents (unlike the
			initial render). It's an inefficient mess but it's the quickest
			possible fix for an apparently urgent problem.
		*/
		FnAttribute::GroupAttribute parents(
			i_location.getAttribute( "dl_light_parents" ) );

		if( parents.isValid() )
		{
			/*
				Export in reverse order to do parents before children, groups
				before transforms. This is coordinated with how the OpScript in
				AddOpLightGatherParents() adds items. It was easier to reverse
				here than in lua.
			*/
			for( int c = parents.getNumberOfChildren() - 1; c >= 0; --c )
			{
				FnAttribute::GroupAttribute item( parents.getChildByIndex(c) );
				ExportSG::ExportLiveUpdateItem( i_ctx, item );
			}
		}
	}

	/* This attribute is the best indication I could find of light type. */
	std::string light_type;
	GetString( i_location, "info.gaffer.packageClass", light_type );

	if( light_type == "AreaLightPackage" || 
	    light_type == "DistantLightPackage" ||
	    light_type == "EnvironmentLightPackage" ||
	    light_type == "SkyLightPackage" ||
	    light_type == "IncandescenceLightPackage" ||
	    light_type == "MeshLightPackage" ||
	    light_type == "PointLightPackage" ||	    
	    light_type == "SpotLightPackage" )
	{
		/*
			For live render updates, we get the material in a separate update
			(the material one) so we'll only create the attributes node to do
			the manual material assignment. It's important not to call
			ExportOneMaterial as it will delete the previous material and we
			can't be certain there's a material update coming next to replace
			it. For example, adopting a light for edit in a gaffer will send
			the light update but not the material one.
		*/
		std::string material_handle = light_name + "|material";
		if( i_location.IsLiveUpdate() )
		{
			i_ctx.m_nsi.Create( material_handle, "attributes" );
		}
		else
		{
			ExportMaterial::ExportOneMaterial( i_ctx, i_location, "material" );
		}

		if( light_type == "AreaLightPackage" )
		{
			if( i_location.IsLiveUpdate() )
			{
				/* Delete previous shape in case the nsi node type changes. */
				i_ctx.m_nsi.Delete( light_name + "|leaf" );
			}

			std::string shape( "square" );
			GetString( i_location, "geometry.shape", shape );

			ExportGeo::ExportLightShape( i_ctx, i_location, shape );
		}
		else if(
			light_type == "DistantLightPackage" ||
			light_type == "EnvironmentLightPackage" ||
			light_type == "SkyLightPackage" )
		{
			// Export this location's transform
			ExportSG::ExportGroup( i_ctx, i_location );

			// Export the environment node as |leaf 
			std::string env_handle = light_name + "|leaf";
			i_ctx.m_nsi.Create( env_handle, "environment" );
			i_ctx.m_nsi.Connect( env_handle, "", light_name, "objects" );

			/* Distant light also has an angle. */
			double angle = -1.0;
			GetDouble( i_location, "geometry.angularDiameter", angle );
			if( angle >= 0.0 )
			{
				i_ctx.m_nsi.SetAttribute(
					env_handle, NSI::DoubleArg( "angle", angle ) );
			}
		}
		else if( light_type == "IncandescenceLightPackage" )
		{
			std::string set_handle = i_location.getFullName() + "|set";
			if( i_location.IsLiveUpdate() )
			{
				/* Delete set used for incandescence control. */
				i_ctx.m_nsi.Delete( set_handle );
			}
			/*
				For the sake of uniformity, export the location itself as a
				group. Makes the transition between modes in live render easier
				too.
			*/
			ExportSG::ExportGroup( i_ctx, i_location );
			i_ctx.m_nsi.Create( set_handle, "set" );
			i_ctx.m_nsi.Connect( set_handle, "", light_name, "objects" );
			/* Connect the set to the target geometry to handle multilight. */
			std::string sourceMesh;
			GetString( i_location, "geometry.sourceMesh", sourceMesh );
			if( !sourceMesh.empty() )
			{
				/*
					The CEL was turned into a space separated list of paths by a
					terminal Op (see AddOpIncandescenceLightCEL). Parse that.
				*/
				std::string::size_type begin = 0, end = sourceMesh.find( ' ' );
				while( true )
				{
					std::string m = sourceMesh.substr( begin, end - begin );
					/*
						The target location might not have been exported yet so
						we'll create it here. I think we can safely assume it
						to be a transform. All scene graph locations are, when
						used without a "|suffix".
					*/
					i_ctx.m_nsi.Create( m, "transform" );
					i_ctx.m_nsi.Connect( m, "", set_handle, "objects" );

					if( end == std::string::npos )
						break;
					begin = end + 1;
					end = sourceMesh.find( ' ', begin );
				}
			}
		}
		else if( light_type == "MeshLightPackage" )
		{
			if( i_location.IsLiveUpdate() )
			{
				/*
					Delete previous leaf node in case it is no longer there. I
					think this could happen when switching from a mesh to a
					group as a source.
				*/
				i_ctx.m_nsi.Delete( light_name + "|leaf" );
			}
			/*
				Export any mesh directly on this location. If the source is a
				group, the geometry will be children.
			*/
			ExportGeo::ExportPolymesh( i_ctx, i_location );
		}
		else if( light_type == "SpotLightPackage" )
		{
			/* Export this location's transform. */
			ExportSG::ExportGroup( i_ctx, i_location );

			/* Export the spot as a single tiny quad. */
			std::string spot_handle = light_name + "|leaf";
			i_ctx.m_nsi.Create( spot_handle, "mesh" );
			i_ctx.m_nsi.Connect( spot_handle, "", light_name, "objects" );

			/*
				Spot light has a radius to do soft shadows, sort of. Scaling
				the spot does the same but leads a huge spot in the viewer.
				1e-3 is what we used before this attribute was added. It is
				also the default now.
			*/
			double radius = 1e-3;
			GetDouble( i_location, "geometry.spotRadius", radius );

			float sz = radius;
			float P[12] = {
				-sz, sz, 0.0f,
				sz, sz, 0.0f,
				sz, -sz, 0.0f,
				-sz, -sz, 0.0f
			};

			NSI::ArgumentList spotArgs;
			i_ctx.m_nsi.SetAttribute(
				spot_handle,
				(
					NSI::IntegerArg( "nvertices", 4 ),
					*NSI::Argument( "P" )
						.SetType( NSITypePoint )
						->SetCount( 4 )
						->CopyValue( P, sizeof(P) )
				) );
		}
		else if( light_type == "PointLightPackage" )
		{
			/* Export this location's transform. */
			ExportSG::ExportGroup( i_ctx, i_location );

			double radius = 1e-3;
			GetDouble( i_location, "geometry.pointRadius", radius );

			/* Export the point as a particle. */
			std::string point_handle = light_name + "|leaf";
			i_ctx.m_nsi.Create( point_handle, "particles" );
			i_ctx.m_nsi.Connect( point_handle, "", light_name, "objects" );

			float P[3] = { 0.0f, 0.0f, 0.0f };
			i_ctx.m_nsi.SetAttribute(
				point_handle,
				(
					*NSI::Argument( "P" )
						.SetType( NSITypePoint )
						->SetValuePointer( &P[0] ),
					NSI::FloatArg( "width", radius*2 )
				) );
		}
	}
}

}
