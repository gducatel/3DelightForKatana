/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2017                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#ifndef __AttributeUtils_h
#define __AttributeUtils_h

#include "FnRender/plugin/GlobalSettings.h"
#include "FnScenegraphIterator/FnScenegraphIterator.h"

#include <cstring>

namespace AttributeUtils
{
	/*
		This is a utility class to build an attribute's name from several
		components, with utility functions to get the values.

		For example, to get a string attribute:
		std::string value = "default";
		GetString( iterator, AttrPath( "name1.name2", subname ), value );
		The equivalent katana API code would fill half your screen.

		I did some benchmarking on attribute lookups and here's what I found:
		- The initial FnScenegraphIterator::getAttribute() call is the most
		  costly. So if several attributes are to be fetched within the same
		  group, it is worth keeping a reference on that group with
		  FnAttribute::GroupAttribute. This could just be because that initial
		  lookup always goes through a std::string.
		- It is, surprisingly, nearly twice faster to do getAttribute(
		  "a.b.c.d" ) than to go through the intermediate GroupAttribute. This
		  suggests either a flat storage in katana or high overhead for the API
		  calls. It could also perhaps be allocation overhead for the
		  intermediate group attributes.
		- The attribute lookups are still easily slowed down by the time it
		  takes to build a std::string from many parts. This means they are
		  fairly fast.
		- The whole thing is still quite fast. For reference, I can do about 3M
		  top level lookups per second on my machine. About 3x that for
		  children of a group.

		In light of this, this class isn't as useful as I thought it would be
		but it still saves some typing so I'm leaving it around for now.
	*/
	class AttrPath
	{
		AttrPath( const AttrPath& );
		void operator=( const AttrPath& );

	public:
		~AttrPath()
		{
			if( m_buffer != m_static_buffer )
				delete[] m_buffer;
		}

		AttrPath( const char *n0 )
		:
			m_path( n0 ),
			m_buffer( 0 )
		{
		}

		AttrPath( const char *n0, const char *n1 )
		{
			size_t s0 = std::strlen( n0 );
			size_t s1 = std::strlen( n1 );
			AllocBuffer( s0 + s1 + 2u );
		
			memcpy( m_buffer, n0, s0 );
			m_buffer[s0] = '.';
			memcpy( m_buffer + s0 + 1u, n1, s1 + 1u );
		}

		AttrPath( const char *n0, const char *n1, const char *n2 )
		{
			size_t s0 = std::strlen( n0 );
			size_t s1 = std::strlen( n1 );
			size_t s2 = std::strlen( n2 );
			AllocBuffer( s0 + s1 + s2 + 3u );
		
			memcpy( m_buffer, n0, s0 );
			m_buffer[s0] = '.';
			memcpy( m_buffer + s0 + 1u, n1, s1 );
			m_buffer[s0 + s1 + 1u ] = '.';
			memcpy( m_buffer + s0 + s1 + 2u, n2, s2 + 1u );
		}

		const char* c_str() const { return m_path; }

	private:
		void AllocBuffer( size_t s )
		{
			if( s < sizeof(m_static_buffer) )
			{
				m_buffer = m_static_buffer;
			}
			else
			{
				m_buffer = new char[s];
			}
			m_path = m_buffer;
		}

	private:
		const char *m_path;
		char m_static_buffer[60];
		char *m_buffer;
	};

	void GetString(
		const FnAttribute::Attribute &i_attr,
		std::string &io_value );

	inline
	void GetString(
		const FnAttribute::GroupAttribute &i_base,
		const AttrPath &i_path,
		std::string &io_value )
	{
		GetString( i_base.getChildByName( i_path.c_str() ), io_value );
	}

	template<typename T>
	void GetString(
		const T &i_base,
		const AttrPath &i_path,
		std::string &io_value )
	{
		GetString( i_base.getAttribute( i_path.c_str() ), io_value );
	}

	void GetInteger(
		const FnAttribute::Attribute &i_attr,
		int &io_value );

	inline
	void GetInteger(
		const FnAttribute::GroupAttribute &i_base,
		const AttrPath &i_path,
		int &io_value )
	{
		GetInteger( i_base.getChildByName( i_path.c_str() ), io_value );
	}

	template<typename T>
	void GetInteger(
		const T &i_base,
		const AttrPath &i_path,
		int &io_value )
	{
		GetInteger( i_base.getAttribute( i_path.c_str() ), io_value );
	}

	void GetFloat(
		const FnAttribute::Attribute &i_attr,
		float &io_value );

	inline
	void GetFloat(
		const FnAttribute::GroupAttribute &i_base,
		const AttrPath &i_path,
		float &io_value )
	{
		GetFloat( i_base.getChildByName( i_path.c_str() ), io_value );
	}

	template<typename T>
	void GetFloat(
		const T &i_base,
		const AttrPath &i_path,
		float &io_value )
	{
		GetFloat( i_base.getAttribute( i_path.c_str() ), io_value );
	}

	void GetDouble(
		const FnAttribute::Attribute &i_attr,
		double &io_value );

	inline
	void GetDouble(
		const FnAttribute::GroupAttribute &i_base,
		const AttrPath &i_path,
		double &io_value )
	{
		GetDouble( i_base.getChildByName( i_path.c_str() ), io_value );
	}

	template<typename T>
	void GetDouble(
		const T &i_base,
		const AttrPath &i_path,
		double &io_value )
	{
		GetDouble( i_base.getAttribute( i_path.c_str() ), io_value );
	}

}

#endif
