/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2017                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/

#ifndef __XGenNSICallbacks_h
#define __XGenNSICallbacks_h

#include <algorithm>
#include <cstring>
#include <stdio.h>
#include <string>
#include <vector>

#include "FnScenegraphIterator/FnScenegraphIterator.h"

#include <XGen/XgRenderAPI.h>
#include <XGen/XgRenderAPIUtils.h>

class ExportContext;
class SGLocation;

using namespace XGenRenderAPI;

//
// Process the render camera attributes and present them as strings formatted
// as required by XGen for the various irRencerCam user attributes.
//
class XGenCameraInfo
{
public:
	XGenCameraInfo(
		ExportContext& i_ctx,
		const FnKat::FnScenegraphIterator &i_root );
	~XGenCameraInfo();

	const char* getFOVAsString() const;
	const char* getRatioAsString() const;
	const char* getPositionAsString() const;
	const char* getTransformAsString() const;

private:
	std::string m_fovAsString;
	std::string m_ratioAsString;
	std::string m_positionAsString;
	std::string m_transformAsString;
};

//
// XGen callbacks for primitve generation during the Katana rendering process.
//
class XGenNSICallbacks : public ProceduralCallbacks
{
public:
	XGenNSICallbacks( 
		ExportContext& i_ctx, 
		const std::string& i_nsiNodeName,
		const XGenCameraInfo& i_cameraInfo );

	virtual ~XGenNSICallbacks();

	virtual void flush(  const char* in_geom, PrimitiveCache* in_cache );
	virtual void log( const char* in_str );

	virtual bool get( EBoolAttribute ) const;
	virtual const char* get( EStringAttribute ) const;
	virtual float get( EFloatAttribute ) const;
	virtual const float* get( EFloatArrayAttribute ) const;
	virtual unsigned int getSize( EFloatArrayAttribute ) const;

	virtual const char* getOverride( const char* in_name ) const;

	virtual bool getArchiveBoundingBox( const char* in_filename, bbox& out_bbox ) const;

	virtual void getTransform( float in_time, mat44& out_mat ) const;

private:
	void flushSplines( const char *geomName, PrimitiveCache* pc );
	void flushCards( const char *geomName, PrimitiveCache* pc );
	void flushSpheres( const char *geomName, PrimitiveCache* pc );
	void flushArchives( const char *geomName, PrimitiveCache* pc );

	ExportContext& m_ctx;
	const std::string& m_nsiNodeName;
	const XGenCameraInfo& m_cameraInfo;
};
#endif

