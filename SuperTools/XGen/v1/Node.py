# vim: set softtabstop=4 expandtab shiftwidth=4:
"""
/******************************************************************************/
/*                                                                            */
/*    Copyright (c)The 3Delight Developers. 2016                              */
/*    All Rights Reserved.                                                    */
/*                                                                            */
/******************************************************************************/
"""

from Katana import NodegraphAPI, Utils
import logging
import re
import os

_3DELIGHT_RENDERER_NAME = "dl"

log = logging.getLogger(_3DELIGHT_RENDERER_NAME.title() + "XGen.Node")

class XGenNode(NodegraphAPI.SuperTool):
    def __init__(self):
        super(XGenNode, self).__init__()
        self.hideNodegraphGroupControls()
        self.addOutputPort('out')

        # Hidden version parameter to detect out-of-date internal networks
        # and upgrade it.
        self.getParameters().createChildNumber('version', 1)

        # Parameter for parent location of all the ponies.
        self.getParameters().createChildString('location', '/root/world/geo')
        self.getParameters().createChildString('file', '')
        self.getParameters().createChildNumber('density', 0.1)

        self.BuildDefaultNetwork()

    def BuildDefaultNetwork(self):
        # Create a merge node
        merge_node = NodegraphAPI.CreateNode('Merge', self)

        # Save the merge node
        self.AddNodeReferenceParam('node_merge', merge_node)

        # Connect the merge node to this node
        node_port = self.getOutputPortByIndex(0)
        merge_port = merge_node.getOutputPortByIndex(0)
        self.getReturnPort(node_port.getName()).connect(merge_port)

    def AddXGenObject(
            self, filename, palette, description, patch, geom, xgenlibdir):
        Utils.UndoStack.OpenGroup(
                'Add XGen object to node "%s"' % self.getName())
        try:
            # Form the name of future node
            xgen_name = description + '_' + patch

            merge_node = self.GetMergeNode()
            xgen_in = NodegraphAPI.CreateNode('XGen_In', self)
            merge_port = merge_node.addInputPort(xgen_name)
            xgen_in.getOutputPortByIndex(0).connect(merge_port)

            name_param = xgen_in.getParameter('name')
            name_param.setExpression(
                    "getParent().location + '/' + %r" % xgen_name)
            name_param.setExpressionFlag(True)

            density_param = xgen_in.getParameter('density')
            density_param.setExpression("getParent().density")
            density_param.setExpressionFlag(True)

            # Set parameters
            xgen_in.getParameter('file').setValue(filename, 0)
            xgen_in.getParameter('palette').setValue(palette, 0)
            xgen_in.getParameter('description').setValue(description, 0)
            xgen_in.getParameter('patch').setValue(patch, 0)
            xgen_in.getParameter('geom').setValue(geom, 0)
            xgen_in.getParameter('xgenlibdir').setValue(xgenlibdir, 0)
            xgen_in.getParameter('frametime').setExpression('frame')

            # self.cleanupInternalNetwork()
            return (name_param, merge_port.getIndex())
        finally:
            Utils.UndoStack.CloseGroup()

    def RemoveXGenObjects(self):
        merge_node = self.GetMergeNode()

        # Delete all the connected nodes and ports
        for i in reversed(range(merge_node.getNumInputPorts())):
            port = merge_node.getInputPortByIndex(i)

            if port:
                out_port = port.getConnectedPort(0)
                if out_port:
                    # Delete the node
                    node = out_port.getNode()
                    node.delete()

            # Delete the port
            merge_node.removeInputPort(port.getName())

    def LayoutInputNodes(self):
        node = self.GetMergeNode()
        spacing = (200, 100)
        pos = NodegraphAPI.GetNodePosition(node)
        x = pos[0] - spacing[0] * (node.getNumInputPorts() - 1) / 2
        y = pos[1] + spacing[1]
        for port in node.getInputPorts():
            if port.getNumConnectedPorts():
                inputNode = port.getConnectedPort(0).getNode()
                NodegraphAPI.SetNodePosition(inputNode, (x, y))
            x += spacing[0]

    def Upgrade(self):
        if not self.isLocked():
            Utils.UndoStack.DisableCapture()
            try:
                pass
                # This is where you would detect an out-of-date version:
                #    node.getParameter('version')
                # and upgrade the internal network.
            except Exception as exception:
                log.exception(
                    'Error upgrading %sXGen node "%s": %s' % (
                        _3DELIGHT_RENDERER_NAME,
                        self.getName(),
                        str(exception)))
            finally:
                Utils.UndoStack.EnableCapture()
        else:
            log.warning(
                'Cannot upgrade locked %sXGen node "%s".'%(
                    _3DELIGHT_RENDERER_NAME, self.getName()) )

    def SetXGenFile(self, filename):
        self.getParameter('file').setValue(filename, 0)
        self.RemoveXGenObjects()
        self.ParseXGenFile(filename)
        self.LayoutInputNodes()

    def GetXGenFile(self):
        return self.getParameter('file').getValue(0)

    def AddNodeReferenceParam(self, paramName, node):
        # Save a node to the parameter of current node. Se need it to thrack it
        # even if the saved node is renamed.
        param = self.getParameter(paramName)
        if not param:
            param = self.getParameters().createChildString(paramName, '')

        # Katana tracks expression. So if we save the node name in the
        # expression, Katana should change it if user renamed the node
        param.setExpression('getNode(%r).getNodeName()' % node.getName())

    def GetMergeNode(self):
        merge_node = self.getParameter('node_merge')
        if not merge_node:
            return None
        return NodegraphAPI.GetNode(merge_node.getValue(0))

    def ParseXGenFile(self, xgenfile):
        # Usually alembic is in the same directory with the same name or the
        # name is written in the xgen file.
        abcfile = os.path.splitext(xgenfile)[0] + '.abc'

        # Parse the xgenfile and get all the objects
        with open(xgenfile) as f:
            current_section = None
            description = None
            palette = None
            descriptions = []
            xgenlibdir = ''

            for line in f.readlines():
                # Remove \t at begin and \n at the end of line
                line = line.rstrip().lstrip()

                # Skip empty
                if not line:
                    continue

                # Get XGen path from comments
                if line.startswith('# Version:'):
                    xgenlibdir = line[10:].lstrip()

                # Skip comments
                if line[0] == '#':
                    continue

                # If it's a single word, then it's a section
                if re.match(r'\A[\w-]+\Z', line):
                    section = line.lower()

                    if section == 'endpatches':
                        description = None
                        current_section = None
                    elif not section.startswith('end'):
                        # It's not a section if it starts with 'end'
                        current_section = section
                    continue

                # It's a parameter that contains name and value separated by \t
                split = line.find('\t')
                if split < 0:
                    continue

                # Lower case
                left = line[:split].lower()
                # Remove \t at begin
                right = line[split:].lstrip()

                if left == 'patches':
                    # It's actually a section with patches
                    current_section = left

                    # It looks like 'description2\t0', we dont need 0 at the end
                    desc_patch = right.split('\t')[0]

                    # Check if the description was defined
                    if desc_patch in descriptions:
                        description = desc_patch

                    continue

                if left == 'name':
                    if current_section == 'palette':
                        palette = right
                        continue

                    if current_section == 'description':
                        descriptions.append(right)
                        continue

                    if current_section == 'patches':
                        if current_section and palette and description:
                            # Bingo we found that
                            self.AddXGenObject(
                                    xgenfile,
                                    palette,
                                    description,
                                    right,
                                    abcfile,
                                    xgenlibdir )

