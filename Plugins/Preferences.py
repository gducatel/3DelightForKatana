import ConfigurationAPI_cmodule as Configuration
import os

def PrefChanged(eventType, eventID, prefKey, prefValue):
	if '3Delight/' not in prefKey[0:9]: return
	Configuration.set(prefKey, str(prefValue))

def _settingToString(v):
	if hasattr(v, 'toString'):
		# Katana 3.0 returns a QVariant.
		return str(v.toString())
	else:
		# Katana 3.1 returns the primitive type directly (eg. bool)
		return str(v)

def buildPrefs3DelightUI():
	"""
	Builds UI for the 3Delight preferences section.
	"""

	try:
		from Katana import KatanaPrefs, Utils
	except ImportError:
		return

	Utils.EventModule.RegisterEventHandler(PrefChanged, 'pref_changed')

	KatanaPrefs.declareGroupPref('3Delight')
	KatanaPrefs.declareBoolPref('3Delight/progressiveRefinement', False,
		'Set Yes to render the image in a progressive fashion.')
	Hints2 = {'widget': 'mapper', 'options': [('Horizontal', '0'),
		('Vertical', '1'), ('Zigzag', '2'), ('Spiral', '3'),
		('Circle', '4')]}
	KatanaPrefs.declareStringPref('3Delight/scanning', 'Circle',
		'Specifies in what order the buckets are rendered.', hints=Hints2)
	Hints3 = {'widget': 'mapper', 'options': [('3delight Display', '0'),
		('Katana Monitor', '1'), ('Both', '2')]}
	KatanaPrefs.declareStringPref('3Delight/renderView', 'Both',
		'Specifies which Render View is used or are used.', hints=Hints3)
	KatanaPrefs.declareBoolPref('3Delight/use3DelightCloud', False,
		'Set Yes to use 3Delight cloud for rendering.')
	settings = KatanaPrefs._KatanaPrefsObject__sessionSettings

	# Call capitalize to have False or True like the one sent to PrefChanged
	value = _settingToString(settings.value('3Delight/progressiveRefinement')).capitalize()
	Configuration.set('3Delight/progressiveRefinement', value)
	value = _settingToString(settings.value('3Delight/scanning'))
	# KatanaPrefs seems to store first value of mapper (i.e. 'Circle'
	# instead of '4') if no Prefs file has been read...
	if not value.isdigit(): value = '4'
	Configuration.set('3Delight/scanning', value)
	value = _settingToString(settings.value('3Delight/renderView'))
	# KatanaPrefs seems to store first value of mapper (i.e. 'Both'
	# insted of '2') if no Prefs file has been read...
	if not value.isdigit(): value = '2'
	Configuration.set('3Delight/renderView', value)
	value = _settingToString(settings.value('3Delight/use3DelightCloud')).capitalize()
	Configuration.set('3Delight/use3DelightCloud', value)
	value = _settingToString(settings.value('3Delight/textureDirectory'))
	Configuration.set('3Delight/textureDirectory', value)

buildPrefs3DelightUI()
