################################################################################
##                                                                            ##
##    Copyright (c)The 3Delight Developers. 2016                              ##
##    All Rights Reserved.                                                    ##
##                                                                            ##
################################################################################

from Katana import Callbacks

def onStartupComplete(objectHash):
    # Write resources/Python folder to python paths
    import os
    import sys
    import platform

    if platform.system() == "Windows":
        separator = ";"
        dynlib = "dll"
    else:
        separator = ":"
        dynlib = "so"

    katana_resources = os.environ["KATANA_RESOURCES"]

    resources = None

    for i in katana_resources.split(separator):
        if os.path.isfile(i + "/Libs/3Delight_for_Katana." + dynlib):
            resources = i
            break

    if resources:
        sys.path.insert(0, resources + '/Python')
    else:
        print "Can't find 3Delight folder"

    # Load the feedback server
    import dlExternalCommandServer

Callbacks.addCallback(Callbacks.Type.onStartupComplete, onStartupComplete)

# vim: set softtabstop=4 expandtab shiftwidth=4:
