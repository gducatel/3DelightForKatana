# Set DEVROOT to (hopefully) the location of this file.
if [[ -z $DEVROOT_3DFK ]]; then
	export DEVROOT_3DFK=`pwd`
endif

# Set PLAT to something platform-specific (matches 3Delight's setting)
if [[ -z $PLAT ]]; then
	export PLAT=`uname -s`-`uname -m | sed -e's/ /-/g'`
fi

# 3Delight home directory where various stuff is needed for the build.
if [[ -z $HOME_3DELIGHT ]]; then
	export HOME_3DELIGHT=/net1/3delight
fi
